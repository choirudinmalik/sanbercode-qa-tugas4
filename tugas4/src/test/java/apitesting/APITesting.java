package apitesting;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import static io.restassured.RestAssured.*;
import static io.restassured.module.jsv.JsonSchemaValidator.*;
import static org.hamcrest.Matchers.*;

import java.util.HashMap;
import java.util.Map;

public class APITesting {
    @BeforeClass
    public void setup(){
        RestAssured.baseURI = "https://reqres.in/api";
    }

    @Test
    public void listUsers(){
        given()
            .param("page", 2).
        when()
            .get("/users")
        .then()
            .statusCode(200)
            .body(matchesJsonSchemaInClasspath("user_schema.json"))
        .log().all();
    }

    @Test 
    public void getUser(){
        get("/users/2")
        .then()
            .statusCode(200)
            .body("data.id", equalTo(2))
            .body("data.email", equalTo("janet.weaver@reqres.in"))
            .body("data.first_name", equalTo("Janet"))
            .body("data.last_name", equalTo("Weaver"))
            .body("data.last_name", notNullValue())
        .log().all();
    }

    @Test
    public void createUser(){
        // JSON Request
        Map<String, String> user = new HashMap<>();
        user.put("name", "Messi");
        user.put("job", "Football Player");

        // Test
        given()
            .contentType("application/json")
            .body(user)
        .when()
            .post("/users")
        .then() 
            .statusCode(201)
            .body("id", notNullValue())
            .body("name", equalTo("Messi"))
            .body("job", equalTo("Football Player"))
            .body("createdAt", notNullValue())
        .log().all();
    }

    @Test
    public void updateUser(){
        // JSON Request
        Map<String, String> user = new HashMap<>();
        user.put("name", "Messi");
        user.put("job", "Goatherd");

        // Test
        given()
            .contentType("application/json")
            .body(user)
        .when()
            .put("/users/2")
        .then() 
            .statusCode(200)
            .body("id", nullValue())
            .body("name", equalTo("Messi"))
            .body("job", equalTo("Goatherd"))
            .body("updatedAt", notNullValue())
        .log().all();
    }

    @Test
    public void deleteUser(){
        given()
        .when()
            .delete("/users/2")
        .then() 
            .statusCode(204)
        .log().all();
    }
}

